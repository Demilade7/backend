// var bodyParser = require( 'body-parser' );
// app.use( bodyParser.urlencoded({ extended: true }) );
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    bcrypt = require('bcrypt-nodejs');

passport.use(new LocalStrategy(function(username, password, done){
    User.findOne({
        email: username
    }, function(err, user){
        if (err){
            return done(err);
        }

        if (!user){
            return done(null, false, {message: 'Incorrect credentials!'});
        }

        bcrypt.compare(password, user.password, function(err, res){
            if (!res){
                return done(null, false, {message: 'Incorrect credentials!'});
            }
            return done(null, user, "Success");
        });
    });
}));