/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var generic_type = {
  type: 'string',
  required: 'true'
};

var bcrypt = require('bcrypt-nodejs');

module.exports = {
  connection: 'someMongodbServer',
  schema: true,

  attributes: {
    firstName: generic_type,
    lastName: generic_type,
    email: {
      type: 'string',
      unique: 'true',
      required: 'true'
    },
    password: generic_type,
    toJSON: function(){
      var obj = this.toObject();
      delete obj.password;
      return obj;
    },
    phoneNum: generic_type
  },
  beforeCreate: function (user, cb){
    bcrypt.genSalt(10, function(err, salt){
      bcrypt.hash(user.password, salt, null, function(err, hash){
        if (err){
          console.log(err);
        }
        else {
          user.password = hash;
        }
        cb();
      });
    });
  },
  beforeUpdate: function (user, cb){
    bcrypt.genSalt(10, function(err, salt){
      bcrypt.hash(user.password, salt, null, function(err, hash){
        if (err){
          console.log(err);
        }
        else {
          user.password = hash;
        }
        cb();
      });
    });
  }
};

