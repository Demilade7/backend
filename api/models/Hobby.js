/**
 * Hobby.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  connection: 'someMongodbServer',
  schema: true,
  attributes: {
    name: {
      type: 'string',
      required: true
    },
    userID: {
      type: 'string',
      required: true
    }
  }
};

