/**
 * HobbyController
 *
 * @description :: Server-side logic for managing hobbies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

//Email Block

const nodemailer = require('nodemailer');
const sparkPostTransport = require('nodemailer-sparkpost-transport');
const transporter = nodemailer.createTransport(sparkPostTransport({
  'sparkPostApiKey': process.env.SPARKPOST_KEY
}))
  
var mailOptions = {
    from: 'test@lanre.co',
    to: '',
    subject: '',
    text: ''
};
  
var sendMail = function(mailOptions){
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log("EMAIL ERROR!!!!!");
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
}
//End of Email Block

//Begin SMS Block
const accountSid = process.env.ACCOUNT_SID;
const authToken = process.env.AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

var sendText = function(recipientNumber, textContent){
    client.messages.create({
        from: '+1 626-325-0829 ',
        to: recipientNumber,
        body: textContent
    }, function(err, message){
        if (err){
            console.log(err);
            return;
        }
    });
}

//End of SMS Block

module.exports = {
	insert: function(req, res){
        var newHobby = req.body;
        
        User.findOne({'id': req.body.userID}).exec(function(err, thisUser){ //Check to see if the user exists
            if (err){
                res.status(403);
                return res.send({message: info.message});
            }
            if (thisUser == null){
                res.status(403);
                return res.send({message: "User does not exist"});
            }
            Hobby.create(newHobby).exec(function(err, createdHobby){
                if (err){
                    res.status(403);
                    return res.send({message: info.message});
                }
                var phoneNum = thisUser.phoneNum;
                var fullName = thisUser.firstName + " " + thisUser.lastName;
                var userEmail = thisUser.email;
                var textContent = "Dear " + fullName + ", You just added a new Hobby named " + createdHobby.name;
                mailOptions.subject = 'Newly Added Hobby';
                mailOptions.to = userEmail;
                mailOptions.text = textContent;
                sendText(phoneNum, textContent);
                sendMail(mailOptions);

                res.status(200);
                return res.json(createdHobby);
            });
        });
    },
    delete: function(req, res){
        var delHobby = '';
        Hobby.findOne({'id': req.body.id}).exec(function(err, foundHobby){
            if (err){
                res.status(403);
                return res.send({message: info.message});
            }
            delHobby = foundHobby.name; //Save the name of the hobby for notification purposes
        });

        Hobby.destroy(req.body).exec(function(err, destroyedHobby){
            if (err){
                res.status(403);
                return res.send({message: info.message});
            }

            if (destroyedHobby.length == 0){
                return res.notFound();
            }
            
            User.findOne({'id': req.body.userID}).exec(function(err, thisUser){
                if (err){
                    res.status(403);
                    return res.send({message: info.message});
                }
                var phoneNum = thisUser.phoneNum;
                var fullName = thisUser.firstName + " " + thisUser.lastName;
                var userEmail = thisUser.email;
                var textContent = "Dear " + fullName + ", You just deleted a Hobby named " + delHobby;
                mailOptions.subject = 'Deleted Hobby';
                mailOptions.to = userEmail;
                mailOptions.text = textContent;
                sendText(phoneNum, textContent);
                sendMail(mailOptions);
                
                return res.ok();
            });
            
        });
    },
    edit: function(req, res){
        var oldHobby = '';
        Hobby.findOne({'id': req.body.id}).exec(function(err, foundHobby){
            if (err){
                res.status(403);
                return res.send({message: info.message});
            }
            oldHobby = foundHobby.name; //Save the name of the hobby for notification purposes
            
        });

        Hobby.update({'id': req.body.id}, {'name': req.body.name}).exec(function(err, newHobby){
            if (err){
                res.status(403);
                return res.send({message: info.message});
            }
            
            User.findOne({'id': req.body.userID}).exec(function(err, thisUser){
                if (err){
                    res.status(403);
                    return res.send({message: info.message});
                }

                var phoneNum = thisUser.phoneNum;
                var fullName = thisUser.firstName + " " + thisUser.lastName;
                var userEmail = thisUser.email;
                var textContent = "Dear " + fullName + ", You just changed " + oldHobby + " to " + newHobby[0].name;
                mailOptions.subject = 'Edited Hobby';
                mailOptions.to = userEmail;
                mailOptions.text = textContent;

                sendText(phoneNum, textContent);
                sendMail(mailOptions);
                
                res.status(200);
                return res.json(newHobby[0]);
            });
        });
    },
    selectAll: function(req, res){
        Hobby.find({'userID': req.param('userID')}).exec(function(err, hobbyList){
            if (err){
                res.status(403);
                return res.send({message: info.message});
            }
            res.status(200);
            return res.json(hobbyList);
        });
    }
};

