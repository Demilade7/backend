/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
// var bodyParser = require( 'body-parser' );
// var express = require('express');
// var app = express()
// app.use( bodyParser.urlencoded({ extended: true }) );

var passport = require('passport');
module.exports = {
	signin: function (req, res){
        passport.authenticate('local', function(err, user, info){
            if (err){
                res.status(403);
                return res.send({message: info.message});
            }
            if (!user){
                res.status(200);                
                return res.send({message: info.message});
            }
                        
            res.status(200);
            return res.json({'id': user.id, successful:true});
        })(req, res);
    },
    signup: function(req, res){
        User.findOne({'email': req.body.email}).exec(function(err, foundUser){
            if (err){
                res.status(403);
                return res.send({message: info.message});
            }
            if (foundUser){
                res.status(200);
                return res.send({message: 'Email already used'});
            }

            User.create(req.body).exec(function(err, newUser){
                if (err){
                    res.status(403);
                    return res.send({message: info.message});
                }
                res.status(200);
                return res.json({'id': newUser.id, successful:true});
            })

        });
    }
};

